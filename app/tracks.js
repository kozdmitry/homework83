const express = require("express");
const Tracks = require("../models/Track");

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const track = await Tracks.find().populate("album", "title", );
        return res.send(track);

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", async (req, res) => {
    const trackInfo = req.body;
    try {
        const track = new Tracks(trackInfo);
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(400).send(e);
    }
    return res.sendStatus(500);
});

module.exports = router;
