const mongoose = require("mongoose");

const ArtistSchema = new mongoose.Schema ({
    title: {
        type: String,
        required: true,
    },
    image: {
        type: String,
    },
    description: {
        type: String,
    }
});

const Artist = mongoose.model('Artist', ArtistSchema);
module.exports = Artist;